const { src, dest, parallel, series, watch } = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify-es').default;
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const cleancss = require('gulp-clean-css');
const fileinclude = require('gulp-file-include');
const del = require('del');


function browsersync () {
    browserSync.init ({
        server: { baseDir: 'build/' },
        notify: false,
        online: true
    })
}

function styles () {
    return src('src/styles/main.scss')
        .pipe(sass())
        .pipe(concat('app.min.css'))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
        .pipe(cleancss({
            level: {
                1: {specialComments: 0}
            }
        }))
        .pipe(dest('build/styles/'))
        .pipe(browserSync.stream())
}

function html () {
    return src('src/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('build/'))

}

function scripts() {
    return src('src/js/*.js')
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(dest('build/js'))
        .pipe(browserSync.stream())
}

function images () {
    return src('src/images/**/*')
        .pipe(newer('build/images/'))
        .pipe(imagemin())
        .pipe(dest('build/images/'))
}

function startwatch () {
    watch('src/styles/**/*', styles);
    watch('src/js/**/*', scripts);
    watch('src/images/**/*', images);
    watch('src/*.html', html);
    watch('build/**/*').on('change', browserSync.reload);
}

function cleanbuild () {
    return del('build/**/*', { force: true })
}

exports.browsersync = browsersync;
exports.styles = styles;
exports.html = html;
exports.scripts = scripts;
exports.images = images;
exports.cleanbuild = cleanbuild;
exports.build = series(cleanbuild, styles, scripts, images, html);
exports.default = parallel(exports.build, browsersync, startwatch);
