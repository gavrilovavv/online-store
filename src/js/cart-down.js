(function () {
    "use strict";

    let shoppingBasket = document.getElementById('shop-basket');
    let cartDown = document.getElementById('cart-down');

    shoppingBasket.addEventListener('click', function (event) {
        event.stopPropagation();
        cartDown.classList.add('shopping-basket__cart-down_active');
    });

    document.addEventListener('click', function () {
        cartDown.classList.remove('shopping-basket__cart-down_active');
    });
})();
